#!/usr/bin/python3 -EOO
# -*- coding: UTF-8 -*-

import os
import time
# import cgi
# import cgitb
# import subprocess
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.firefox.service import Service as FirefoxService

# cgitb.enable()
# print(cgi.test())

# fields = cgi.FieldStorage()
# print("Content-type: text/html\n")
# print(fields['token'].value)
# print(os.environ['REQUEST_URI'])

# Instructions: just clone the repository in a web served folder
# Meta-Press.es will behave differently if it is running as a web page or a WebExtension
# With Firefox:
# - https://intoli.com/blog/firefox-extensions-with-selenium/
# - download the latest geckodriver: https://github.com/mozilla/geckodriver/releases
# - download latest Firefox developper edition (it must be the developper edition)
# Put those at HOME + FF_PATH folder
HOME = os.environ['HOME']
FF_PATH='/ff'

# Package the WebExtension as an XPI file: make backend_xpi
# Set a MP_LOCAL_XPI web context environment variable pointing to the created .xpi file
# I've done it via an .htaccess and those instructions :
# SetEnv MP_LOCAL_XPI /var/www/[…]/2022-07-10_22:32:54-meta-press-ext_7zip.xpi
# SetEnv HOME /var/www/[…]/


try:
	ff_opt = FirefoxOptions()
	ff_opt.binary = HOME+FF_PATH+'/firefox/firefox'
	ff_opt.headless = True
	ff_service = FirefoxService(executable_path=HOME+FF_PATH+'/geckodriver')
	driver = webdriver.Firefox(service=ff_service, options=ff_opt)
	driver.install_addon('../../', temporary=True)
	# driver.get('https://www.meta-press.es')
	# Selenium wait
	# https://stackoverflow.com/questions/15122864/selenium-wait-until-document-is-ready
	wait = WebDriverWait(driver, 10)
	wait.until(EC.number_of_windows_to_be(2))
	driver.switch_to.window(driver.window_handles[1])
	ext_id = driver.current_url.split('moz-extension://')[1].split('/html/welcome.html')[0]
	# print('ext_id', ext_id)
	driver.get('moz-extension://{}/html/index.html?{}&submit=1'.format(
		ext_id,
		os.environ['QUERY_STRING']))
	# wait.until(EC.number_of_windows_to_be(3))
	# driver.switch_to.window(driver.window_handles[2])
	# print(driver.title)
	print("Content-type: application/json\n")
	# open file "{}.json" % fields['token'].value
	# print it back to client
	while(True):
		export_content = driver.execute_script(
			"return document.getElementById('backend_export_content').textContent")
		if (not export_content):
			time.sleep(0.5)
		else:
			print(export_content)
			break
finally:
	try:
		# driver.close()  # only closes the current tab
		driver.quit()
	except Exception:
		pass


# Remains of my attempts to get Chromium working as backend web browser
#
# git clone https://github.com/scheib/chromium-latest-linux/
# ./update.sh

# On graphical user desktop with a running chromium fitted with a local dev version of
# meta-press
# tar czf chromium_profile.tgz ~/.config/chromium
# scp chromium_profile.tgz remote.server.url:

# On server
# tar xzf chromium_profile.tgz
# vi Profile\ 1/Preferences
# -> update path to local webextention dev files (path to meta-press-ext)

# Tried options :
# --incognito  ## webext are blocked in incognito mode
# --headless --disable-gpu --dump-dom URL  ## can't get it working

"""
chromium_command = 'xvfb-run -w 0 -a -e /dev/stdout {} --no-sandbox --disable-gpu --user-data-dir={} "chrome-extension://{}/html/index.html?{}&submit=1"'.format(
		os.environ['CHROMIUM_BIN'],
		os.environ['CHROME_USER_DATA_DIR'],
		os.environ['LOCAL_CHROMIUM_META_PRESS_EXT_ID'],
		os.environ['QUERY_STRING'])
# xvfb-run -e /dev/stdout -a

print("chromium_command", chromium_command)
p = subprocess.Popen(chromium_command, stdout=subprocess.PIPE, shell=True)
(output, err) = p.communicate()
p_status = p.wait()
print("exit code : ", p_status)
print("Command error output : ", err)
print("Command standard output : ", output.decode())
"""
