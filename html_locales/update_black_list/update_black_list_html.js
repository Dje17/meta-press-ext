// SPDX-Filename: ./update_black_list_html.js
// SPDX-FileCopyrightText: 2022 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

import * as µ from '/js/BOM_utils.js'
import * as bl from '/html_locales/update_black_list/update_black_list.js'
async function init() {
	const bl_file = await fetch('/html_locales/update_black_list/black_list_manual_part.json')
	let bl_json = await bl_file.json()
	const src_file = await fetch('/json/sources.json')
	const src_json = await src_file.json()
	bl_json = bl.enumerate_sources(src_json, bl_json)
	const bl_str = JSON.stringify(bl_json, null, '\t')
	µ.upload_file_to_user('black_list.json', bl_str)
	µ.id('txta').innerText = bl_str
	// setTimeout(() => window.close(), 250)  // how to know when file is written ?
}
init()
