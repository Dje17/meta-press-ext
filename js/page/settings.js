// SPDX-FileName: ./settings.js
// SPDX-FileCopyrightText: 2017-2022 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
//
/* globals browser List Choices */
// import * as _ from '/js/js_utils.js'
import * as µ from '/js/BOM_utils.js'
import * as mµ from '/js/mp_utils.js'
import * as ss from '/js/scheduled_searchs.js'
import * as g from '/js/gettext_html_auto.js/gettext_html_auto.js'

mµ.set_theme()

/**
 * setting_page.js
 * @module setting_page
 */

/**
 * The translation module configured with the user language.
 */
let mp_i18n, stored_tz
/**
 * The list of shedule search.
 */
let sp_req ={
	autoSearchList: new List('automatic_search', {
		item: 'sch_sea_item',
		valueNames: ['sch_sea_query', 'sch_sea_nb','sch_sea_last_run',
			{ name: 'sch_sea_url', attr: 'value' },
			{ name: 'sch_sea_id', attr: 'id' },
			{ name: 'input_date', attr: 'value' },
			{ name: 'input_HHMM', attr: 'value' },
			{ name: 'min_date', attr: 'min' },
			{ name: 'title_1', attr: 'title'},
			{ name: 'title_2', attr: 'title'}
		]
	})
}
let select_opt = {
	resetScrollPosition: false,
	duplicateItemsAllowed: false,
	searchResultLimit: 8,
	shouldSort : false,
	searchFields: ['label']
}
/**
 * Set the timezone choice (select)
 * From	https://stackoverflow.com/questions/39263321/javascript-get-html-timezone-dropdown
 * @async
 * @param {*} mp_i18n The translation module
 */
async function set_tz_choices(mp_i18n) {
	let element = document.getElementById('mp_tz')
	if (!Intl.supportedValuesOf) {
		let option = document.createElement('option')
		option.text = mp_i18n.gettext('Unsupported')
		option.value = null
		element.add(option)
		return
	}
	for(let tz of Intl.supportedValuesOf('timeZone')) {
		let option = document.createElement('option')
		option.text = tz
		option.value = tz
		element.add(option)
	}
	let nav_date = new Date()
	let nav_tz = nav_date.getTimezoneOffset()
	let default_option = document.createElement('option')
	default_option.text = mp_i18n.gettext('Your navigator timezone')
	default_option.value = nav_tz
	element.add(default_option, 0)
}
// document.getElementById('purges_c_src').onclick = rm_provided_overrided_src
/**
 * Remove the old custom sources overriding the provided sources based on the version number.
 */
async function rm_provided_overrided_src() {  // eslint-disable-line no-unused-vars
	let old = {}
	let to_rm = []
	let custom_src = await mµ.get_custom_src()
	if (custom_src && custom_src !== {}) {
		let mp_version = await mµ.get_manifest().version
		for (const i of Object.keys(old))
			if (old[i].mp_version !== mp_version) to_rm.push(i)
	}
	if (window.confirm(mp_i18n.gettext('Do you want to remove your custom source(s) '
		+ 'that are also defined in the provided sources now ?') + '\n' + to_rm.toString())) {
		for (const i of to_rm) delete old[i]
		custom_src = JSON.stringify(old, null, '\t')
		mµ.to_storage('custom_src', custom_src)
		// update_source_objs("", true)  // not accessible here, defined in js/meta-press.js:2827
	}
}
//
// scheduled search
//
async function schedule_search(s) {
	let table_sch_s = Array.isArray(s) ? s : await ss.init_table_sch_s(s)
	let table_sch_s_length = table_sch_s.length
	if (table_sch_s_length) {
		document.getElementById('automatic_search').style.display = 'block'
		document.querySelector('#automatic_search .list').style.visibility = 'visible'
		sp_req.autoSearchList.clear()  // to remove the template
	}
	for(let i=table_sch_s_length;i--;) {
		const id = `sch_sea__${i}`
		const sch_sea_date = await ss.set_timezone(new Date(), stored_tz).toISOString()
		let intlNum = Intl.NumberFormat('fr', {minimumIntegerDigits: 2, useGrouping: 0})
		let local_url = new URL(window.location).origin
		let new_url = new URL(local_url + table_sch_s[i])
		let [list_p, params] = await mµ.set_text_params(new_url, mp_i18n)
		let last_run = new_url.searchParams.get('last_run') //is a string (Marin)
		last_run = last_run === '0' ? 0 : new Date(last_run)
		let last_run_hm, last_run_ymd
		if(last_run !== 0 && !isNaN(last_run)) {
			last_run = await ss.set_timezone(last_run, stored_tz)
			last_run_hm = intlNum.format(
				last_run.getHours())   + ':' + intlNum.format(last_run.getMinutes())
			last_run_ymd = last_run.getFullYear() +'-'+ intlNum.format(
				last_run.getMonth()+1) + '-' + intlNum.format(last_run.getDate())
		}
		let run_freq = new_url.searchParams.get('run_freq')
		if (!ss.run_frequencies.includes(run_freq))
			throw new Error(`Bad run_freq ${run_freq}`)
		let next_run_ymd = '', next_run_hm = ''
		if ('sch_s_stop' !== run_freq) {
			let next_run = new_url.searchParams.get('next_run')
			next_run = await ss.sch_sea_maj_date(next_run, run_freq, stored_tz)
			if (next_run) {
				new_url = new URL(local_url + '/html/index.html' + new_url.search)
				new_url.searchParams.set('next_run', next_run.toUTCString())
				new_url.searchParams.set('id_sch_s', id)
				table_sch_s[i] = new_url.search
				let next_tz = await ss.set_timezone(next_run, stored_tz)
				next_run_hm = intlNum.format(
					next_tz.getHours()) + ':' + intlNum.format(next_tz.getMinutes())
				next_run_ymd = next_tz.getFullYear() +'-'+ intlNum.format(
					next_tz.getMonth()+1) + '-'+ intlNum.format(next_tz.getDate())
			}
		}
		sp_req.autoSearchList.add({
			sch_sea_nb: i,
			sch_sea_url: new_url,
			sch_sea_query: list_p.q,
			title_1 : params,
			title_2 : params,
			sch_sea_last_run: last_run === 0 ? 0 : last_run_ymd+' '+last_run_hm,
			sch_sea_id: id,
			input_date: next_run_ymd,
			input_HHMM: next_run_hm,
			min_date: sch_sea_date.slice(0,10)
		})
		document.getElementById(id).parentElement.querySelector('.frq').value = run_freq
	}
	add_all_events(table_sch_s)
	sp_req.autoSearchList.sort('sch_sea_id', { order: 'asc' })
	browser.alarms.clearAll()
	tab = table_sch_s
	if (tab && tab.length)
		await ss.create_alarm([tab])
}
function show_sch_s_save_btn_cb (evt) {
	let local_save_btn = evt.target.parentElement.querySelector('.save_date')
	local_save_btn.style.visibility = 'visible'
}
function add_all_events(table_sch_s) {
	for(let i=table_sch_s.length; i--;) {
		const local_url = new URL(window.location).origin
		const new_url = new URL(local_url + table_sch_s[i])
		const id = `sch_sea__${i}`
		let elt = document.getElementById(id)
		if (!elt) continue
		elt = elt.parentElement
		elt.querySelector('.frq').addEventListener('change', () => {
			save_as_date(id, table_sch_s)})
		let save_btn = elt.querySelector('.save_date')
		elt.querySelector('.input_date').addEventListener('change', show_sch_s_save_btn_cb)
		elt.querySelector('.input_HHMM').addEventListener('change', show_sch_s_save_btn_cb)
		save_btn.addEventListener('click', (evt) => {
			save_as_date(id, table_sch_s)
			let local_save_btn = evt.target.parentElement.querySelector('.save_date')
			local_save_btn.style.visibility = 'hidden'
		})
		elt.querySelector('.suppr_sch_sea').addEventListener('click', () => {
			del_sch_sea(elt, table_sch_s)})
		elt.querySelector('.sch_sea_url').addEventListener('click', (evt) => {
			browser.tabs.create({ 'url': evt.target.value })
		})
		elt.querySelector('.play').addEventListener('click', () => {
			ss.launch_sch_s(table_sch_s[i])
		})
		elt.querySelector('.duplicate').addEventListener('click', () => {
			new_url.searchParams.delete('id_sch_s')
			sp_req.autoSearchList.clear()
			add_elt(new_url, table_sch_s)
		})
	}
}
async function add_sch_sea(store) {
	let table_sch_s = await ss.init_table_sch_s(store)
	add_elt(store.new_sch_sea, table_sch_s)
}
let m_id_sch_s, m_value_sch_s //value of modified schedule search  use in background.js
async function add_elt(new_sch_sea, table_sch_s) {
	let d = new Date()
	let next_run = new Date(d.getFullYear(), d.getMonth(), d.getDate()+1, 0, 0).toUTCString()
	if(new_sch_sea) {
		let new_url = new URL(new_sch_sea)
		let id = new_url.searchParams.get('id_sch_s')
		if(!new_url.searchParams.get('next_run'))
			new_url.searchParams.set('next_run',next_run)
		if(!new_url.searchParams.get('last_run'))
			new_url.searchParams.set('last_run','0')
		if(!new_url.searchParams.get('run_freq'))
			new_url.searchParams.set('run_freq','sch_s_stop')
		if(id) {
			id = id.split('__')[1]
			table_sch_s.splice(id,1,new_url.search)
		} else {
			id = table_sch_s.length
			new_url.searchParams.set('id_sch_s',`sch_sea__${id}`)
			table_sch_s.push(new_url.search)
		}
		m_id_sch_s = `sch_sea__${id}`
		m_value_sch_s = table_sch_s[id]
		await mµ.to_storage(m_id_sch_s, m_value_sch_s)
		mµ.del_storage('new_sch_sea')
		schedule_search(table_sch_s)
	}
}
function sch_sea_hook (a) {
	if(a.new_sch_sea)
		add_sch_sea(a)
	else
		schedule_search(a)
}
async function del_sch_sea(html_elt, table_sch_s){
	html_elt = html_elt.querySelector('td')
	let del_id = html_elt.id.split('__')[1] //is a string
	del_id = parseInt(del_id) //Marin et christopher
	let del_url = table_sch_s[del_id]
	sp_req.autoSearchList.clear()
	if(del_id == table_sch_s.length-1) { //if last element delete it in browser storage
		mµ.del_storage(`sch_sea__${del_id}`)
	} else {
		for(let i=del_id; i<table_sch_s.length-1; i++) {// for all element after
			m_id_sch_s = `sch_sea__${i}`
			m_value_sch_s = table_sch_s[i+1]
			//decremente value of id
			m_value_sch_s = m_value_sch_s.replace(/sch_sea__[0-9]*/, `sch_sea__${i}`)
			await mµ.to_storage(m_id_sch_s, m_value_sch_s)
			mµ.del_storage(`sch_sea__${i+1}`)//remove old element with bad id
		}
	}
	sch_sea_hook(await mµ.get_storage())
	browser.alarms.clear(del_url)
}
let tab
async function save_as_date(id, table_sch_s) {
	let new_id = id.split('__')[1]
	let local_url = new URL(window.location).origin
	let new_url = new URL(local_url + table_sch_s[new_id])
	let elt = document.getElementById(id).parentElement
	let new_date = elt.querySelector('.input_date').value
	let new_hours = elt.querySelector('.input_HHMM').value
	let new_run_freq = elt.querySelector('.frq').value
	new_date = new_date.split('-')
	let year = new_date[0]
	let month = new_date[1]
	let day = new_date[2]
	new_hours = new_hours.split(':')
	let hours = new_hours[0]
	let minute = new_hours[1]
	// let next_run = new_url.searchParams.get('next_run')
	let next_run = new Date(year, month-1, day, hours, minute, 0)
	next_run = await ss.set_timezone(next_run, stored_tz, false)
	if(!elt.querySelector('.input_date:invalid')) {
		new_url.searchParams.set('next_run', next_run.toUTCString())
		new_url.searchParams.set('run_freq', new_run_freq)
		elt.querySelector('.sch_sea_url').value = local_url + '/html/index.html'+new_url.search
		table_sch_s[new_id] = new_url.search
		browser.alarms.clearAll()
		m_id_sch_s = id
		m_value_sch_s = table_sch_s[new_id]
		await mµ.to_storage(m_id_sch_s, m_value_sch_s)
		tab = table_sch_s
		if (tab && tab.length)
			await ss.create_alarm([tab])
	}
}

async function init() {
	const userLang = await mµ.get_wanted_locale()
	document.body.classList.add('javascript_loaded')
	const QUERYSTRING = new URL(window.location).searchParams
	if (QUERYSTRING.get('xgettext'))
		await g.xgettext_html()
	mp_i18n = await g.gettext_html_auto(userLang)
	if (typeof browser !== 'undefined')
		sch_sea_hook(await mµ.get_storage())
	await set_tz_choices(mp_i18n)
	stored_tz = await mµ.get_tz()
	//
	// general settings
	//
	let lang_select = document.getElementById('mp_lang')
	lang_select.value =  await mµ.get_locale()
	lang_select.addEventListener('change', () => {
		mµ.to_storage('locale', µ.get_HTML_select_value(lang_select))
		//localStorage.setItem('locale', µ.get_HTML_select_value(lang_select))
		location.reload()
	})
	let tz_select = document.getElementById('mp_tz')
	let choice_tz = new Choices(tz_select, Object.assign(select_opt))
	/**
	 * The current timezone.
	 */
	let tz = await mµ.get_tz()
	choice_tz.setChoiceByValue(tz)
	tz_select.addEventListener('change', async() => {
		let tz_value = await µ.get_HTML_select_value(tz_select)
		mµ.to_storage('tz', tz_value)
		location.reload()
	})

	let bg_select = document.getElementById('dark_background')
	bg_select.value = await mµ.get_theme()
	bg_select.addEventListener('change', () => {
		mµ.to_storage('dark_background', µ.get_HTML_select_value(bg_select))
		location.reload()
	})
	let live_search_reload = document.getElementById('live_search_reload')
	live_search_reload.checked = await mµ.get_live_search_reload()
	live_search_reload.addEventListener('change', () => {
		mµ.to_storage('live_search_reload', live_search_reload.checked)
	})
	let sentence_search = document.getElementById('sentence_search')
	sentence_search.checked = await mµ.get_sentence_search()
	sentence_search.addEventListener('change', () => {
		mµ.to_storage('sentence_search', sentence_search.checked)
	})
	let max_res_by_src = document.getElementById('max_res_by_src')
	max_res_by_src.value = await mµ.get_max_res_by_src()
	max_res_by_src.addEventListener('change', () => {
		mµ.to_storage('max_res_by_src', max_res_by_src.value)
	})
	let undup_results = document.getElementById('undup_results')
	undup_results.checked = await mµ.get_undup_results()
	undup_results.addEventListener('change', () => {
		mµ.to_storage('undup_results', undup_results.checked)
	})
	let load_photos = document.getElementById('load_photos')
	load_photos.checked = await mµ.get_load_photos()
	load_photos.addEventListener('change', () => {
		mµ.to_storage('load_photos', load_photos.checked)
	})
	let search_timeout = document.getElementById('search_timeout')
	search_timeout.value = await mµ.get_search_timeout()
	search_timeout.addEventListener('change', () => {
		mµ.to_storage('search_timeout', search_timeout.value)
	})
	let child_mode = document.getElementById('child_mode')
	child_mode.checked = await mµ.get_child_mode()
	child_mode.addEventListener('change', () => {
		mµ.to_storage('child_mode', child_mode.checked)
	})
	let news_loading = document.getElementById('news_loading')
	news_loading.checked = await mµ.get_news_loading()
	news_loading.addEventListener('change', () => {
		mµ.to_storage('news_loading', news_loading.checked)
	})
	let live_news_reload = document.getElementById('live_news_reload')
	live_news_reload.checked = await mµ.get_live_news_reload()
	live_news_reload.addEventListener('change', () => {
		mµ.to_storage('live_news_reload', live_news_reload.checked)
	})
	let max_news_loading = document.getElementById('max_news_loading')
	max_news_loading.value = await mµ.get_max_news_loading()
	max_news_loading.addEventListener('change', () => {
		mµ.to_storage('max_news_loading', max_news_loading.value)
	})
	let max_news_by_src = document.getElementById('max_news_by_src')
	max_news_by_src.value = await mµ.get_max_news_by_src()
	max_news_by_src.addEventListener('change', () => {
		mµ.to_storage('max_news_by_src', max_news_by_src.value)
	})
	let news_page_size = document.getElementById('news_page_size')
	news_page_size.value = await mµ.get_news_page_size()
	news_page_size.addEventListener('change', () => {
		mµ.to_storage('news_page_size', news_page_size.value)
	})
	let keep_host_perm = document.getElementById('keep_host_perm')
	keep_host_perm.checked = await mµ.get_keep_host_perm()
	keep_host_perm.addEventListener('change', () => {
		mµ.to_storage('keep_host_perm', keep_host_perm.checked)
	})
	let provided_sources_json = await fetch('/json/sources.json')
	provided_sources_json = await provided_sources_json.json()
	document.getElementById('request_host_perm').addEventListener('click', () => {
		mµ.request_sources_perm(Object.keys(provided_sources_json), provided_sources_json)
	})
	document.getElementById('drop_host_perm').addEventListener('click', mµ.drop_host_perm)
}
init()
